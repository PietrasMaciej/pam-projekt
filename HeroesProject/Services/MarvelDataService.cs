﻿using HeroesProject.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace HeroesProject
{
    public class MarvelFacade
    {

        private const string PrivateKey = "cca8d8f10358b239316589efcbf11f9244430a3f";
        private const string PublicKey = "768bccea52defc4432698fa49db9c5a1";
        private const string ImageNotAvailablePath = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available";
        private const int MaxCharacters = 1500;

        public static async Task PopulateMarvelCharactersAsync(ObservableCollection<Character> marvelCharacters)
        {
            //zadanie wlasnosci, ktorych nie mozna kontrolowac - Marvel API
            try
            {
                var characterDataWrapper = await GetCharacterDataWrapperAsync();

                var characters = characterDataWrapper.data.results;

                foreach (var character in characters)
                {
                    //filtrowanie bohaterow, ktorzy nie maja zdjec itd.
                    if (character.thumbnail != null && character.thumbnail.path != "" && character.thumbnail.path != ImageNotAvailablePath)
                    {

                        character.thumbnail.small = String.Format("{0}/standard_small.{1}",
                           character.thumbnail.path,
                           character.thumbnail.extension);

                        character.thumbnail.large = String.Format("{0}/portrait_xlarge.{1}",
                            character.thumbnail.path,
                            character.thumbnail.extension);

                        marvelCharacters.Add(character);
                    }
                }
            }
            catch (Exception)
            {
                return;
            } 
        }


        public static async Task PopulateMarvelComicsAsync(int characterId, ObservableCollection<ComicBook> marvelComics)
        {
            //zadanie wlasnosci, ktorych nie mozna kontrolowac - Marvel API
            try
            {
                var comicDataWrapper = await GetComicDataWrapperAsync(characterId);

                var comics = comicDataWrapper.data.results;

                foreach (var comic in comics)
                {
                    //filtrowanie bohaterow, ktorzy nie maja zdjec itd.
                    if (comic.thumbnail != null && comic.thumbnail.path != "" && comic.thumbnail.path != ImageNotAvailablePath)
                    {

                        comic.thumbnail.small = String.Format("{0}/portrait_small.{1}",
                           comic.thumbnail.path,
                           comic.thumbnail.extension);

                        comic.thumbnail.large = String.Format("{0}/portrait_xlarge.{1}",
                            comic.thumbnail.path,
                            comic.thumbnail.extension);

                        marvelComics.Add(comic);
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }


        private static async Task<CharacterDataWrapper> GetCharacterDataWrapperAsync()
        {
            //****konfigurowanie url
            Random random = new Random();
            //losowanie z puli 1500 bohaterów
            var offset = random.Next(MaxCharacters);

            ////generowanie md5 hash
            //var timestamp = datetime.now.ticks.tostring();
            //var hash = createhash(timestamp);

            string url = String.Format("https://gateway.marvel.com:443/v1/public/characters?limit=10&offset={0}", offset);

            ////zapytanie do api Marvel'a
            //HttpClient http = new HttpClient();
            //var response = await http.GetAsync(url);
            //var jsonMessage = await response.Content.ReadAsStringAsync();
            var jsonMessage = await CallMarvelAsync(url);

            //odpowiedz 
            var serializer = new DataContractJsonSerializer(typeof(CharacterDataWrapper));
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonMessage));

            var result = (CharacterDataWrapper)serializer.ReadObject(ms);
            return result;
        }

        //komiksy
        private static async Task<ComicDataWrapper> GetComicDataWrapperAsync(int characterId)
        {
            var url = String.Format("https://gateway.marvel.com:443/v1/public/comics?characters={0}&limit=10", characterId);

            var jsonMessage = await CallMarvelAsync(url);

            //odpowiedz -> string / json -> deserialize
            var serializer = new DataContractJsonSerializer(typeof(ComicDataWrapper));
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonMessage));

            var result = (ComicDataWrapper)serializer.ReadObject(ms);
            return result;
        }

        private async static Task<string> CallMarvelAsync(string url)
        {
            //generowanie MD5 Hash
            var timeStamp = DateTime.Now.Ticks.ToString();
            var hash = CreateHash(timeStamp);

            // string completeUrl = String.Format("{0}&apikey={1}&ts={2}&hash={3}", url, PublicKey, timeStamp, hash);
            string completeUrl = $@"{url}&apikey={PublicKey}&ts={timeStamp}&hash={hash}";

            //zapytanie do api Marvel'a
            HttpClient http = new HttpClient();
            var response = await http.GetAsync(completeUrl);
            return await response.Content.ReadAsStringAsync();

        }

        private static string CreateHash(string timeStamp)
        {
            var toBeHashed = timeStamp + PrivateKey + PublicKey;
            var hashedMessage = ComputeMD5(toBeHashed);
            return hashedMessage;
        }

        public static string ComputeMD5(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }
    }
}
