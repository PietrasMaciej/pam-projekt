﻿using HeroesProject.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using NotificationsExtensions.ToastContent;
using Windows.UI.Notifications;
using NotificationsExtensions.TileContent;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HeroesProject.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<Character> MarvelCharacters { get; set; }
        public ObservableCollection<ComicBook> MarvelComics { get; set; } /*bindowane*/

        public MainPage()
        {
            this.InitializeComponent();

            MarvelCharacters = new ObservableCollection<Character>();
            MarvelComics = new ObservableCollection<ComicBook>();

            IToastImageAndText04 notify = ToastContentFactory.CreateToastImageAndText04();
            notify.TextHeading.Text = "Wróć ponownie i odkryj nowych bohaterów!";
            notify.TextBody2.Text = "HeroesProject";
            ScheduledToastNotification delay;
            delay = new ScheduledToastNotification(notify.GetXml(), DateTime.Now.AddSeconds(2));
            delay.Id = "#Id";
            ToastNotificationManager.CreateToastNotifier().AddToSchedule(delay);


            var template = TileContentFactory.CreateTileSquare150x150PeekImageAndText01();
            template.TextBody1.Text = "Super sloth hero";
            template.Image.Src = "ms-appx:///Assets/sloth.jpg";

            var wideTemlate = TileContentFactory.CreateTileWide310x150PeekImageAndText01();
            wideTemlate.TextBodyWrap.Text = "Super larger sloth hero";
            wideTemlate.Image.Src = "ms-appx:///Assets/sloth.jpg";
            wideTemlate.Square150x150Content = template;

            TileNotification wideNotification = wideTemlate.CreateNotification();
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.Update(wideNotification);

        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            MyProgressRing.IsActive = true;
            MyProgressRing.Visibility = Visibility.Visible;

            while (MarvelCharacters.Count < 10)
            {
                Task waitForFinishedCall = MarvelFacade.PopulateMarvelCharactersAsync(MarvelCharacters);
                await waitForFinishedCall;
            }

            MyProgressRing.IsActive = false;
            MyProgressRing.Visibility = Visibility.Collapsed;
        }

        //po kliknieciu na ikonke bohatera
        private async void MasterListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            MyProgressRing.IsActive = true;
            MyProgressRing.Visibility = Visibility.Visible;

            //Czyszczenie nazwy poprzedniego komiksu
            ComicDetailNameTextBlock.Text = "";
            //Czyszczenie starego opisu komiksu
            ComicDetailDescriptionTextBlock.Text = "";
            //Czyszczenie również popredniego obrazka
            ComicDetailImage.Source = null;

            var selectedCharacter = (Character)e.ClickedItem;

            DetailNameTextBlock.Text = selectedCharacter.name;
            DetailDescriptionTextBlock.Text = selectedCharacter.description;

            var largeImage = new BitmapImage();
            Uri uri = new Uri(selectedCharacter.thumbnail.large, UriKind.Absolute);
            largeImage.UriSource = uri;
            DetailImage.Source = largeImage;

            MarvelComics.Clear();

            await MarvelFacade.PopulateMarvelComicsAsync(selectedCharacter.id, MarvelComics);

            MyProgressRing.IsActive = false;
            MyProgressRing.Visibility = Visibility.Collapsed;
        }

        //po kliknieciu na komiks
        private void ComicsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedComic = (ComicBook)e.ClickedItem;

            ComicDetailNameTextBlock.Text = selectedComic.title;

            if(selectedComic.description != null)
                 ComicDetailDescriptionTextBlock.Text = selectedComic.description;

            var largeImage = new BitmapImage();
            Uri uri = new Uri(selectedComic.thumbnail.large, UriKind.Absolute);
            largeImage.UriSource = uri;
            ComicDetailImage.Source = largeImage;
        }



        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    var data = MarvelFacade.GetCharacterList();
        //}
    }
}
