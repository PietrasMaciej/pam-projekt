﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HeroesProject.Views;
using SQLite.Net.Attributes;

namespace HeroesProject.Views
{
    public sealed partial class DataBase : Page
    {
        SQLiteConnection conn;

        public DataBase()
        {
            this.InitializeComponent();
           // string path = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "db", "heroes.db");
            string path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<Customer>();
        }

        private void Retrieve_Click(object sender, RoutedEventArgs e)
        {
            var query = conn.Table<Customer>();
            string id = "";
            string name = "";
            string age = "";

            foreach (var message in query)
            {
                id = id + " " + message.Id;
                name = name + " " + message.Name;
                age = age + " " + message.Age;
            }

            textBlock2.Text = "ID: " + id + "\nTitle: " + name + "\n#No: " + age;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {

            var s = conn.Insert(new Customer()
            {
                Name = textBox.Text,
                Age = textBox1.Text
            });

        }
    }

    public class Customer
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }
    }

}
