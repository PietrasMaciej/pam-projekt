﻿using GalaSoft.MvvmLight;

namespace HeroesProject.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private bool _isLoading = false;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                RaisePropertyChanged("IsLoading");

            }
        }
        private string _info;
        public string Info
        {

            get
            {
                return _info;
            }
            set
            {
                if (value != _info)
                {
                    _info = value;
                    RaisePropertyChanged("Info");
                }
            }
        }

        public MainPageViewModel()
        {
            Info = "© 2016 Marvel";
        }
    }
}